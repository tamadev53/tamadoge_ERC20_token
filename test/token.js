const Token = artifacts.require("Token");
const { expect, assert } = require('chai');
const truffleAssert = require('truffle-assertions');
const { ethers } = require("hardhat");
const startTime=Date.now()+100000000;
const endTime=startTime+6000000;

describe('Tamadoge', (accounts) => {
    let token, balance;
    let ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

    before(async function () {
        accounts = await web3.eth.getAccounts();

        await truffleAssert.reverts( Token.new("TD","TD","0",startTime,endTime),
        "VM Exception while processing transaction: reverted with reason string 'Invalid mint amount'");
        
        await truffleAssert.reverts( Token.new("TD","TD","14000000000000000000000000000",startTime,endTime),
        "VM Exception while processing transaction: reverted with reason string 'Invalid mint amount'");
        
        await truffleAssert.reverts( Token.new("TD","TD","100000","16564889",endTime),
        "VM Exception while processing transaction: reverted with reason string 'Start time is in the past'");
        await truffleAssert.reverts( Token.new("TD","TD","100000",startTime,"1656488996"),
        "VM Exception while processing transaction: reverted with reason string 'Invalid end time'");
        token = await Token.new("TD","TD","140000000000000000000000000",startTime,endTime);
        assert.equal(await token.balanceOf(accounts[0]),"140000000000000000000000000");
        assert.equal(await token.owner(),accounts[0]);
        assert.equal(parseInt(await token.mintable()),1.26*10**27);
    });

    it('has correct name', async () => {
        const name = await token.name();
        assert.equal(name, 'TD');
    })

    it('has correct symbol', async () => {
        const symbol = await token.symbol();
        assert.equal(symbol, 'TD');
    })

    it('has correct decimals', async () => {
        const decimals = await token.decimals();
        assert.equal(decimals.toNumber(), 18);
    })

    it('has correct owner', async () => {
        const owner = await token.owner();
        assert.equal(owner, accounts[0]);
    })
    
    it('has correct total supply', async () => {
        const supply = await token.totalSupply();
        assert.equal(supply,"140000000000000000000000000");
    })
    it('transfers correct', async() =>{
        await truffleAssert.reverts(token.transfer(ZERO_ADDRESS, 100, {from: accounts[0]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: transfer to the zero address'");

        //await truffleAssert.reverts(token.transfer(accounts[1], 0, {from: accounts[0]}),
        //"VM Exception while processing transaction: reverted with reason string 'Transfer amount must be greater than zero'");

        await truffleAssert.reverts(token.transfer(accounts[1],"1700000000000000000000000000", {from: accounts[0]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: transfer amount exceeds balance'");

        await token.transfer(accounts[1],100);
        assert.equal(await token.balanceOf(accounts[1]),100)
    })

    it('transferFrom works', async() =>{
       await token.transfer(accounts[4],100,{from : accounts[0]});
       await token.approve(accounts[5],50,{from : accounts[4]});
       await truffleAssert.reverts(token.transferFrom(accounts[4],accounts[6],51, {from: accounts[5]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: transfer amount exceeds allowance'");
       await truffleAssert.reverts(token.transferFrom(ZERO_ADDRESS,accounts[6],50, {from: accounts[5]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: transfer from the zero address'");
       await truffleAssert.reverts(token.transferFrom(accounts[4],ZERO_ADDRESS,50, {from: accounts[5]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: transfer to the zero address'");
       await token.approve(accounts[5],150,{from : accounts[4]});
       await truffleAssert.reverts(token.transferFrom(accounts[4],accounts[6],101, {from: accounts[5]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: transfer amount exceeds balance'");
       await token.transferFrom(accounts[4],accounts[6],50,{from : accounts[5]});
       assert.equal(await token.balanceOf(accounts[6]),50);
       assert.equal(await token.allowance(accounts[4],accounts[5]),100);
          
    })

    it('burn works', async() =>{
        await truffleAssert.reverts(token.burn(51, {from: accounts[6]}),
         "VM Exception while processing transaction: reverted with reason string 'ERC20: burn amount exceeds balance'");
         const beforeSupply=await token.totalSupply();
         await token.burn(25,{from : accounts[6]});
         const afterSupply=await token.totalSupply();
         assert.equal(await token.balanceOf(accounts[6]),25);
         assert.equal(beforeSupply.toString(),"140000000000000000000000000")
         assert.equal(afterSupply.toString(),"139999999999999999999999975");
       
        })

    it('approve works', async() =>{

        await truffleAssert.reverts( token.approve(ZERO_ADDRESS,100, {from: accounts[1]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: approve to the zero address'");

        await token.approve(accounts[2],50, {from: accounts[1]});
        assert.equal(await token.allowance(accounts[1],accounts[2]),50);    
    })

    it('increase and decrease allowance ', async() =>{

        await truffleAssert.reverts( token.increaseAllowance(ZERO_ADDRESS,100, {from: accounts[1]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: approve to the zero address'");
        await token.increaseAllowance(accounts[2],50, {from: accounts[1]});
        assert.equal(await token.allowance(accounts[1],accounts[2]),100); 

        await token.decreaseAllowance(accounts[2],50, {from: accounts[1]});
        assert.equal(await token.allowance(accounts[1],accounts[2]),50); 

        await truffleAssert.reverts(token.decreaseAllowance(ZERO_ADDRESS,25, {from: accounts[1]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: decreased allowance below zero'");

        await truffleAssert.reverts( token.decreaseAllowance(accounts[2],100, {from: accounts[1]}),
        "VM Exception while processing transaction: reverted with reason string 'ERC20: decreased allowance below zero'");

        
        
    })

    it('changes owner', async()=>{
        await truffleAssert.reverts(token.transferOwnership(ZERO_ADDRESS),"VM Exception while processing transaction: reverted with reason string 'Ownable: new owner is the zero address'")
        await token.transferOwnership(accounts[3], {from:accounts[0]});
        let owner = await token.owner();
        assert.equal(owner, accounts[3]);
       
    })


    it('mints correct', async()=>{
        await truffleAssert.reverts(token.mint(0, {from: accounts[3]}),
        "VM Exception while processing transaction: reverted with reason string 'Amount out of bounds'");
        const mintableAmount=await token.mintable();
        await truffleAssert.reverts(token.mint(mintableAmount+1, {from: accounts[3]}),
        "VM Exception while processing transaction: reverted with reason string 'Amount out of bounds'");
        await truffleAssert.reverts(token.mint("1134000000000000000000000000",{from : accounts[2]}),
        "VM Exception while processing transaction: reverted with reason string 'Ownable: caller is not the owner'");
        await token.mint("1134000000000000000000000000",{from : accounts[3]});
        assert.equal(await token.balanceOf(accounts[3]),"1134000000000000000000000000");
        assert.equal(parseInt(await token.mintable()),"126000000000000000000000000");
        
    })

    



    


    it("Renounce ownership", async() =>{
        await token.renounceOwnership({from: accounts[3]});
        let owner = await token.owner();
        assert.equal(owner, 0);
    })
})
