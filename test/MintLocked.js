const Token = artifacts.require("Token");
const { expect, assert } = require('chai');
const truffleAssert = require('truffle-assertions');
const { ethers } = require("hardhat");
const startTime=Date.now()+120000;
const endTime=startTime+1200000;

describe('Tamadoge Claim', (accounts) => {
    let token;
    
    before(async function () {
        accounts = await web3.eth.getAccounts();
        token = await Token.new("TD","TD","1400000000000000000000000000",startTime,endTime);
        
    });
    it('claim Works', async () => {
       
       await truffleAssert.reverts(token.mintLockedSupply(),"VM Exception while processing transaction: reverted with reason string 'Nothing to mint'");
       let changeTime=startTime-1;
       await truffleAssert.reverts(token.mintLockedSupply(),"VM Exception while processing transaction: reverted with reason string 'Nothing to mint'");
       let intialBalance=await token.balanceOf(accounts[0]);
       changeTime=changeTime+1
       assert.equal(intialBalance.toString(),"1400000000000000000000000000")
       await ethers.provider.send("evm_setNextBlockTimestamp", [changeTime]);
       await ethers.provider.send('evm_mine');
       await token.mintLockedSupply();
       let afterBalance=await token.balanceOf(accounts[0])
       assert.equal(afterBalance.toString(),'1400000500000000000000000000');
       assert.equal(await token.mintedLockedSupply(),'500000000000000000000');
       changeTime=changeTime+120000;
       await ethers.provider.send("evm_setNextBlockTimestamp", [changeTime]);
       await ethers.provider.send('evm_mine');
       await token.mintLockedSupply();
       afterBalance=await token.balanceOf(accounts[0])
       assert.equal(afterBalance.toString(),'1460000500000000000000000000');
       assert.equal(await token.mintedLockedSupply(),'60000500000000000000000000');
       
       
       changeTime=changeTime+(1200000-1-120000);
       await ethers.provider.send("evm_setNextBlockTimestamp", [changeTime]);
       await ethers.provider.send('evm_mine');
       await token.mintLockedSupply();
       afterBalance=await token.balanceOf(accounts[0])
       assert.equal(afterBalance.toString(),'2000000000000000000000000000'); 
       assert.equal(await token.mintedLockedSupply(),'600000000000000000000000000');
       await truffleAssert.reverts(token.mintLockedSupply(),"VM Exception while processing transaction: reverted with reason string 'Nothing to mint'")
       let totalSupply=await token.totalSupply();
       assert.equal(totalSupply.toString(),afterBalance.toString());
       let hardcap=await token.hardCap();
       assert.equal(hardcap.toString(),totalSupply.toString());

    })
})
